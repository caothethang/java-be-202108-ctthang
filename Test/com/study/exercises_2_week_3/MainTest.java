package com.study.exercises_2_week_3;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;

@RunWith(value = Parameterized.class)
public class MainTest {
    private String email;
    private String phoneNumber;
    private boolean expected;
    
    public MainTest(String email, String phoneNumber, boolean expected) {
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.expected = expected;
    }
    
    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {"abc@gmail", "02918234", false}, {"abc@gmail.com", "", true},
                {"abc@gmail.com", "0376620677", true}, {"abc@gmail.com", "0376620677", false}
        });
    }
    
    @Test
    public void validEmail() {
        assertEquals(expected, Main.validEmail(email));
    }
    
    @Test
    public void validPhone() {
        assertEquals(expected, Main.validPhone(phoneNumber));
    }
}