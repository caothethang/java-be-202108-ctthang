package com.study.exercises46;

import java.util.ArrayList;
import java.util.LinkedList;

public class Album {
    private String name;
    private String artist;
    private ArrayList<Song> songs;
    
    public Album(String name, String artist) {
        this.name = name;
        this.artist = artist;
        this.songs = new ArrayList<Song>();
    }
    // function addSong to arraylist songs
    public boolean addSong(String title, double duration){
        if (this.findSong(title) == null){
            this.songs.add(new Song(title,duration));
            return true;
        }
        return false;
    }
    // function findsong to find song with title param
    private Song findSong(String title){
        for(Song s : songs){
            if(s.getTitle().equals(title)){
                return s;
            }
        }
        return null;
    }
    
    /*
        function addToPlayList add song to playlist with the song that 
        find in songs with trackNumber param
    */
    public boolean addToPlayList(int trackNumber , LinkedList<Song> linkedList){
        if(trackNumber<1 || trackNumber > this.songs.size()){
            return false;
        }
        linkedList.add(this.songs.get(trackNumber-1));
        return true;
    }

    /*
        function addToPlayList add song to playlist with the song that 
        find in songs with title param
    */
    public boolean addToPlayList(String title, LinkedList<Song> list){
        Song songSearched = this.findSong(title);
        if(songSearched == null){
            return false;
        }
        list.add(songSearched);
        return true;
    }
    
}
