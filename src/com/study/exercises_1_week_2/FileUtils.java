package com.study.exercises_1_week_2;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;

/*
    Class for read file
 */
public final class FileUtils {
    //FileUtils instance
    private static FileUtils instance;
    
    /*
        method to create instance
     */
    public static FileUtils getInstance() {
        if (instance == null) {
            instance = new FileUtils();
        }
        return instance;
    }
    
    /*
        private contructor
     */
    private FileUtils() {
    
    }
    
    /*
        Function to read tail of file
        lineNumber : a number line in tail file
        pathFile   : location file to access
     */
    public void readTailFile(int lineNumber, RandomAccessFile raf) {
        try {
            // get the length of file
            long lengthOfFile = raf.length() - 1;
            StringBuilder stringHasRead = new StringBuilder();
            int lineHadRead = 0;   // create a count variable
            for (long filePointer = lengthOfFile; filePointer != - 1; filePointer--) {
                raf.seek(filePointer);      // move pointer to this pointer
                char c = (char) raf.read(); // get the character at this pointer
                String str = String.valueOf(c);
                if (str == "\n" || str == "\r" || str == "\r\n") {            // checking if the character is "\n"
                    lineHadRead = lineHadRead + 1;
                    if (lineHadRead >= lineNumber) {
                        break;
                    }
                }
                stringHasRead.append(c);               // append this character to
            }
            String lineAtTailFile = stringHasRead.reverse().toString();
            System.out.println(lineNumber +" lines at the tail file : ");
            System.out.println(lineAtTailFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    
}
