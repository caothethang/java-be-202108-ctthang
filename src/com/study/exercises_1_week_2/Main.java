package com.study.exercises_1_week_2;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.InputMismatchException;
import java.util.Objects;
import java.util.Scanner;

public class Main {
    private static String pathFile;
    private static int numberOfLine;
    private static Scanner scanner;
    private static RandomAccessFile randomAccessFile;
    
    public static void main(String[] args) throws IOException {
        scanner = new Scanner(System.in);
        FileUtils fileUtils = FileUtils.getInstance();
        getInput();
        fileUtils.readTailFile(numberOfLine, randomAccessFile);
        randomAccessFile.close();
    }
    
    /*
        Function to get value from input
     */
    private static void getInput() {
        System.out.println("Enter your file path : ");
        pathFile = scanner.nextLine();
        while (Objects.isNull(randomAccessFile)) {
            try {
                randomAccessFile = new RandomAccessFile(pathFile, "r");
            } catch (FileNotFoundException e) {
                System.out.println("File isn't exist , enter file path again: ");
                pathFile = scanner.nextLine();
            }
        }
        System.out.println("Enter you number of lines: ");
        int fileLength = getFileLength();
        while (numberOfLine < 1 || numberOfLine > fileLength) {
            try {
                numberOfLine = scanner.nextInt();
                if (numberOfLine > fileLength) {
                    System.out.println("Over the lines of file");
                    System.out.println("Enter your number again : ");
                    
                }
                if (numberOfLine < 1) {
                    System.out.println("Nothing to show with this number");
                    System.out.println("Enter your number again");
                    
                }
            } catch (InputMismatchException e) {
                System.out.println("Input data not valid");
                scanner.nextLine();
                System.out.println("Enter your number again : ");
            }
        }
    }
    
    /*
        Function get line count of line in file
     */
    private static int getFileLength() {
        Path path = Paths.get(pathFile);
        long fileLength = 0;
        try {
            fileLength = Files.lines(path).count();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return (int) fileLength;
    }
}
