package com.study.exercises_3_week_3.Client;

import java.io.DataInputStream;
import java.net.Socket;

/*
    class to get messages from server
 */
class ReadClient extends Thread {
    private final Socket client;
    
    public ReadClient(Socket client) {
        this.client = client;
    }
    
    @Override
    public void run() {
        DataInputStream dis;
        try {
            dis = new DataInputStream(client.getInputStream());
            while (true) {
                String sms = dis.readUTF();
                System.out.println(sms);
            }
        } catch (Exception e) {
            System.out.println("Client is disconnected");
        }
    }
}
