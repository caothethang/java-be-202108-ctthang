package com.study.exercises_3_week_3.Client;

import java.io.DataOutputStream;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

/*
    Class to send messages to server
 */
class WriteClient extends Thread {
    private final Socket client;
    private final String name;
    
    public WriteClient(Socket client, String name) {
        this.client = client;
        this.name = name;
    }
    
    @Override
    public void run() {
        DataOutputStream dos;
        Scanner sc;
        try {
            dos = new DataOutputStream(client.getOutputStream());
            sc = new Scanner(System.in);
            while (true) {
                String sms = sc.nextLine();
                if(sms.equals("exit")){
                    dos.writeUTF("exit");
                    break;
                }
                Date date = new Date();
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                String time = simpleDateFormat.format(date);
                dos.writeUTF("["+time+"] "+name + ": " + sms);
            }
        } catch (Exception e) {
            System.out.println("Checking server it's can be closed");
        }
    }
}
