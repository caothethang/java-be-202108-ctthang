package com.study.exercises_3_week_3.Client;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;

public class Client {
    
    public static void main(String[] args) throws IOException, InterruptedException {
        Scanner sc = new Scanner(System.in);
        Socket client = new Socket("localhost", 5000);
        DataInputStream dataInputStream = new DataInputStream(client.getInputStream());
        System.out.println(dataInputStream.readUTF());
        System.out.print("Enter your nickname: ");
        String name = sc.nextLine();
        WriteClient write = new WriteClient(client, name);
        write.start();
        ReadClient read = new ReadClient(client);
        read.start();
    }
}

