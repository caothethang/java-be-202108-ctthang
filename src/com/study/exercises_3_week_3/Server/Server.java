package com.study.exercises_3_week_3.Server;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class Server {
    // list client are connected to server
    public static ArrayList<Socket> listSK;
    
    public static void main(String[] args) throws IOException {
        Server.listSK = new ArrayList<>();
        ServerSocket server = new ServerSocket(5000);
        System.out.println("Server is listening...");
        while (true) {
            Socket socket = server.accept();
            System.out.println("Connected to client at port: " + socket.getPort());
            WriteServer write = new WriteServer(socket);
            write.start();
            Server.listSK.add(socket);
            ReadServer read = new ReadServer(socket);
            read.start();
        }
    }
    
}



