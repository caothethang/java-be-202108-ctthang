package com.study.exercises_3_week_3.Server;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

/*
    class to send message from server to all client
 */
class WriteServer extends Thread {
    private final Socket client;
    
    public  WriteServer(Socket socket){
        this.client = socket;
    }
    
    @Override
    public void run() {
        DataOutputStream dos;
        String sms = "I'm Admin";
        try {
            dos = new DataOutputStream(client.getOutputStream());
            dos.writeUTF("Server: " + sms);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
}
