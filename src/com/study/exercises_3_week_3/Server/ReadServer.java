package com.study.exercises_3_week_3.Server;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;

/*
    Class to get message from client and forward to other client
 */
class ReadServer extends Thread {
    private final Socket socket;
    
    public ReadServer(Socket socket) {
        this.socket = socket;
    }
    
    @Override
    public void run() {
        try {
            DataInputStream dis = new DataInputStream(socket.getInputStream());
            while (true) {
                String sms = dis.readUTF();
                if (sms.equals("exit")) {
                    Server.listSK.remove(socket);
                    System.out.println("Đã ngắt kết nối với " + socket);
                    dis.close();
                    socket.close();
                    return;
                }
                // forward message to all other client connected
                for (Socket item : Server.listSK) {
                    if (item.getPort() != socket.getPort()) {
                        DataOutputStream dos = new DataOutputStream(item.getOutputStream());
                        dos.writeUTF(sms);
                    }
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
