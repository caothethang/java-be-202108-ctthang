package com.study.exercises50;

import java.util.*;

public class Main {
    private static Map<Integer, Location> locations = new HashMap<Integer, Location>();
    private static final String QUIT = "You are sitting in front of a computer learning Java";
    private static final String ROAD = "You are standing at the end of a road before a small brick building";
    private static final String FOREST = "You are in the forest";
    private static final String VALLEY = "You are in a valley beside a stream";
    private static final String HILL = "You are at the top of a hill";
    private static final String BUILDING = "You are inside a building, a well house for a small spring";
    private static final int QUIT_ID = 0;
    private static final int ROAD_ID = 1;
    private static final int FOREST_ID = 5;
    private static final int VALLEY_ID = 4;
    private static final int HILL_ID = 2;
    private static final int BUILDING_ID = 3;
    private static Scanner scanner = new Scanner(System.in);
    private static Map<String, String> vocabulary;
    private static Map<String, Integer> exits;
    private static int currentLocation = 1;
    
    public static void main(String[] args) {
        createMapAdventure();
        while (true) {
            System.out.println(locations.get(currentLocation).getDescription());
            if (currentLocation == 0) {
                break;
            }
            command();
        }
        scanner.close();
    }
    
    public static void createMapAdventure() {
        locations.put(QUIT_ID, new Location(QUIT_ID, QUIT));
        locations.put(ROAD_ID, new Location(ROAD_ID, ROAD));
        locations.put(HILL_ID, new Location(HILL_ID, HILL));
        locations.put(BUILDING_ID, new Location(BUILDING_ID, BUILDING));
        locations.put(VALLEY_ID, new Location(VALLEY_ID, VALLEY));
        locations.put(FOREST_ID, new Location(FOREST_ID, FOREST));
        // exit for ROAD
        locations.get(1).addExit("W", HILL_ID);
        locations.get(1).addExit("E", BUILDING_ID);
        locations.get(1).addExit("S", VALLEY_ID);
        locations.get(1).addExit("N", FOREST_ID);
        // exit for HILL
        locations.get(2).addExit("N", FOREST_ID);
        // exit for BUILDING
        locations.get(3).addExit("W", ROAD_ID);
        // exit for VALLEY
        locations.get(4).addExit("N", ROAD_ID);
        locations.get(4).addExit("W", HILL_ID);
        // exit for FOREST
        locations.get(5).addExit("S", ROAD_ID);
        locations.get(5).addExit("W", HILL_ID);
        //
        vocabulary = new HashMap<>();
        vocabulary.put("QUIT", "Q");
        vocabulary.put("NORTH", "N");
        vocabulary.put("SOUTH", "S");
        vocabulary.put("WEST", "W");
        vocabulary.put("EAST", "E");
    }
    
    /*
        Print way map
     */
    public static void printWay(Map<String, Integer> map) {
        for (String key : map.keySet()) {
            System.out.print(key + ",");
        }
    }
    
    /*
        function get input and move
     */
    public static void command() {
        exits = getExits();
        System.out.print("Available exits are ");
        printWay(exits);
        System.out.println();
        String directionInput = scanner.nextLine().toUpperCase();
        String direction = null;
        if (directionInput.length() > 1) {
            String[] words = directionInput.split(" ");
            for (String word : words) {
                if (vocabulary.containsKey(word)) {
                    direction = vocabulary.get(word);
                    break;
                }
            }
        }
        if (directionInput.length() == 1) {
            moveToLocation(directionInput);
        } else {
            moveToLocation(direction);
        }
    }
    
    /*
        function for move to location on map
     */
    public static void moveToLocation(String direction) {
        exits = getExits();
        if (exits.containsKey(direction)) {
            currentLocation = exits.get(direction);
        } else {
            System.out.println("You can't go in that direction");
        }
    }
    
    /*
        function to get ways exits on each location
     */
    public static Map<String, Integer> getExits() {
        return locations.get(currentLocation).getExits();
    }
}
