package com.study.exercises_1_week_3;


public class Producer implements Runnable {
    private QueueMessage queueMessage;
    
    public Producer(QueueMessage queueMessage) {
        this.queueMessage = queueMessage;
    }
    
    @Override
    public void run() {
        try {
            while (true) {
                queueMessage.put(new Message("This is a message"));
                Thread.sleep(500);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
