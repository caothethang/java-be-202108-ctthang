package com.study.exercises_1_week_3;

import java.util.LinkedList;
import java.util.concurrent.TimeUnit;

public class QueueMessage {
    private static int compacity = 60;
    private LinkedList<Message> listMessage = new LinkedList<>();
    private static int dequeueCount = 0;
    private static int enqueueCount = 0;
    private static long startTime = System.nanoTime();
    /*
        Function to add a message to queue
     */
    public void put(Message message) throws InterruptedException {
        synchronized (this) {
            while (listMessage.size() == compacity) {
                System.out.println("Queue is full");
                break;
            }
            if (listMessage.size() < compacity) {
                System.out.println("Producer in "+Thread.currentThread().getName() + " put in queue : ");
                listMessage.addLast(message);
                enqueueCount++;
                System.out.println("The current size in queue: " + listMessage.size()+"\n");
//                System.out.println("Inside queue: ");
//                listMessage.forEach(message1 -> System.out.println(message1.toString()));
            }
            if(TimeUnit.SECONDS.convert(System.nanoTime()-startTime,TimeUnit.NANOSECONDS)==60){
                System.out.println("Enqueue : "+enqueueCount +"/"+ "minutes");
                System.out.println("Dequeue : "+ dequeueCount +"/"+ "minutes");
                
            }
        }
    }
    
    /*
        Function to get last element in queue message
     */
    public Message take() throws InterruptedException {
        synchronized (this) {
            while (listMessage.size() == 0) {
                System.out.println("Queue empty");
                wait(1000);
            }
            Message message = listMessage.removeLast();
            dequeueCount++;
            System.out.println("Consumer in "+Thread.currentThread().getName() +" get : " + "message");
            System.out.println("Queue size just has : " + listMessage.size()+"\n");
            if(TimeUnit.SECONDS.convert(System.nanoTime()-startTime,TimeUnit.NANOSECONDS)==60){
                System.out.println("Enqueue: " + enqueueCount +"/"+ "minutes");
                System.out.println("Dequeue: " + dequeueCount +"/"+ "minutes");
            }
            return message;
        }
    }
    
    public synchronized int size() {
        return listMessage.size();
    }
    
}
