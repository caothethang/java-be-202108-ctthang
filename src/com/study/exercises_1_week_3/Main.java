package com.study.exercises_1_week_3;


import java.util.concurrent.TimeUnit;

public class Main {
    public static void main(String[] args) {
        QueueMessage queueMessage = new QueueMessage();
        Producer producer = new Producer(queueMessage);
        Producer producer1 = new Producer(queueMessage);
        Producer producer2 = new Producer(queueMessage);
        Consumer consumer = new Consumer(queueMessage);
        Consumer consumer1 = new Consumer(queueMessage);
        Consumer consumer2 = new Consumer(queueMessage);
        new Thread(producer).start();
        new Thread(producer1).start();
        new Thread(producer2).start();
        new Thread(consumer).start();
        new Thread(consumer1).start();
        new Thread(consumer2).start();
        
    }
}
