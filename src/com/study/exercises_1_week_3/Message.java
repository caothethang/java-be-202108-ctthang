package com.study.exercises_1_week_3;

public class Message {
    private long id;
    private String message;
    
    public Message(String message) {
        this.id = System.currentTimeMillis();
        this.message = message;
    }
    
    @Override
    public String toString() {
        return "Message{" +
                "id=" + id +
                ", message='" + message + '\'' +
                '}';
    }
}
