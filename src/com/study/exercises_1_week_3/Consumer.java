package com.study.exercises_1_week_3;

import java.util.concurrent.TimeUnit;

public class Consumer implements Runnable {
    private QueueMessage queueMessage;
    
    public Consumer(QueueMessage queueMessage) {
        this.queueMessage = queueMessage;
    }
    
    @Override
    public void run() {
        try {
            while (true) {
                Message message = queueMessage.take();
                Thread.sleep(1000);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
