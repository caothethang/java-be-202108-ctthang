package com.study.exercises52;

public class Planet extends HeavenlyBody{
    public Planet(String name, double orbitalPeriod) {
        super(name, orbitalPeriod, BodyTypes.PLANET);
    }
    
    @Override
    public boolean addSatellite(HeavenlyBody heavenlyBody) {
        if(heavenlyBody == null){
            return false;
        }
        if(heavenlyBody.getKey().getBodyType() == BodyTypes.MOON) {
            return super.addSatellite(heavenlyBody);
        } else {
            return false;
        }
    }
    
}
