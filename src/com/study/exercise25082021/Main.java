package com.study.exercise25082021;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static final int MIN_VALUE = 50;
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int numberOfItems = sc.nextInt();         // Số lượng phần tử được nhập từ bàn phím
        sc.nextLine();
        double[] arr = createArray(numberOfItems);
        System.out.println("Mảng vừa tạo: ");
        showArrayElement(arr);
        System.out.println("Các ptu < "+ MIN_VALUE);
        arr = removeElementLessThanFifty(arr);
        System.out.println("Sau khi xoá,số phần tử còn lại: ");
        showArrayElement(arr);
        sc.close();
    }
    
    //Tạo mảng
    public static double[] createArray(int num) {
        double[] arr = new double[num];
        for (int i = 0; i < num; i++) {
            double random = Math.random() * 100;
            arr[i] = random;
        }
        return arr;
    }
    
    /*
        Xoá phần tử trong mảng nhỏ hơn 50
     */
    public static double[] removeElementLessThanFifty(double[] arr) {
        List<Double> arrRemoved = new ArrayList<>();
        for (int i = 0; i < arr.length; i++) {
            // Những phần tử thoả mãn điều kiện ta add vào arraylist
            if (arr[i] >= MIN_VALUE) {
                arrRemoved.add(arr[i]);
            } else {
                System.out.println("Phần tử nhỏ hơn 50: " + arr[i]);
            }
        }
        // Một mảng mới chứa những phần tử ở trong arraylist
        double[] arrResult = new double[arrRemoved.size()];
        int lengthOfNewArray = arrRemoved.size();
        for (int i = 0; i < lengthOfNewArray; i++) {
            arrResult[i] = arrRemoved.get(i);
        }
        arrRemoved.clear();
        return arrResult;
    }
    /*
        Hiển thị các phần tử trong mảng
     */
    public static void showArrayElement(double[] arr){
        for (int i = 0; i < arr.length; i++) {
            System.out.println(arr[i]);
        }
    }
}
