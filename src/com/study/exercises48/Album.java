package com.study.exercises48;

import java.util.ArrayList;
import java.util.LinkedList;

public class Album {
    private String name;
    private String artist;
    private SongList songs;
    
    public Album(String name, String artist) {
        this.name = name;
        this.artist = artist;
        songs = new SongList();
    }
    /*
        add Song object to songs in inner class with title and duration param
     */
    public boolean addSong(String title, double duration) {
        if (this.songs.findSong(title) == null) {
            songs.add(new Song(title, duration));
            return true;
        }
        return false;
    }
    
    /*
        function addToPlayList add song to playlist with the song that
        find in songs with trackNumber param
    */
    public boolean addToPlayList(int trackNumber, LinkedList<Song> linkedList) {
        if (trackNumber < 1 || trackNumber > this.songs.songs.size()) {
            return false;
        }
        linkedList.add(this.songs.findSong(trackNumber));
        return true;
    }
    
    /*
        function addToPlayList add song to playlist with the song that
        find in songs with title param
    */
    public boolean addToPlayList(String title, LinkedList<Song> list) {
        Song songFound = this.songs.findSong(title);
        if (songFound == null) {
            return false;
        }
        list.add(songFound);
        return true;
    }
    
    /*
        Inner class to hold song
     */
    public static class SongList {
        private ArrayList<Song> songs;
        
        private SongList() {
            this.songs = new ArrayList<>();
        }
        
        /*
            add Song object with to songs list
         */
        private boolean add(Song s) {
            if (this.songs.contains(s)) {
                return false;
            }
            this.songs.add(s);
            return true;
        }
        
        /*
            findsong and return Song object with title
         */
        private Song findSong(String title) {
            for (Song s : songs) {
                if (s.getTitle().equals(title)) {
                    return s;
                }
            }
            return null;
        }
        
        /*
            findsong and return Song object with tracknumber
         */
        private Song findSong(int trackNumber) {
            if (trackNumber < 1 || trackNumber > songs.size()) {
                return null;
            }
            return songs.get(trackNumber - 1);
        }
    }
}
