package com.study.exercises2608;

public class Main {
    public static void main(String[] args) {
        ArrayUtils arrayUtils = ArrayUtils.getInstance();
        Integer[] arr1 = new Integer[]{1,2,3,4};
        Integer[] arr2 = new Integer[0];
        Integer[] arr3= null;
        arrayUtils.display(arr1);
        arrayUtils.display(arr2);
        arrayUtils.display(arr3);
    }
}
