package com.study.exercises2608;

import java.util.Objects;

public class ArrayUtils {
    
    private static ArrayUtils INSTANCE;
    
    /*
        function to create instance of ArrayUtils
     */
    public static ArrayUtils getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new ArrayUtils();
        }
        return INSTANCE;
    }
    
    /*
        Private constructor
     */
    private ArrayUtils() {
    }
    
    /*
            function to display array with generic
         */
    public <T> void display(T[] arr) {
        if (Objects.isNull(arr) || arr.length < 1) {
            System.out.println("Không hợp lệ");
        } else {
            for (int i = 0; i < arr.length; i++) {
                System.out.println(arr[i]);
            }
        }
    }
}
