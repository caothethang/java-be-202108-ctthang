package com.study.exercises_2_week_3;

import java.util.Scanner;
import java.util.regex.Pattern;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Nhập vào email: ");
        String email = scanner.nextLine();
        while (!validEmail(email)) {
            System.out.println("Email không hợp lệ: ");
            System.out.println("Nhập lại email: ");
            email=scanner.nextLine();
        }
        System.out.println("Email hợp lệ ");
        System.out.println("Nhập vào số điện thoại:");
        String phoneNumber = scanner.nextLine();
        while (!validPhone(phoneNumber)) {
            System.out.println("Số điện thoại không hợp lệ: ");
            System.out.println("Nhập lại số điện thoại: ");
            phoneNumber=scanner.nextLine();
        }
        System.out.println("Số điện thoại hợp lệ ");
    }
    /*
        Function to check if email is valid
     */
    public static boolean validEmail(String email) {
        // bắt đầu bằng chữ cái,theo sau có thể là chữ hoặc số
        // bắt buộc phải có @
        // đằng sau là tên miền có thể là abc.xyz.edu và là các cụm ngăn cách bởi dấu chấm,mỗi cụm có độ dài > 2
        String regexForEmail = "^[a-zA-Z][\\w]+@([\\w]+\\.[\\w]+|[\\w]+\\.[\\w]{2,}\\.[\\w]{2,})$";
        Pattern pattern = Pattern.compile(regexForEmail);
        return pattern.matcher(email).matches();
    }
    /*
        Function to check if phone number is valid
        
     */
    public static boolean validPhone(String phoneNumber) {
        // bắt đầu bằng 84 hoặc 03,05,07,08,09
        // cụm số theo sau có độ dài = 8 , không được nhỏ 0 hoặc lớn hơn 9
        String regexForPhone = "(84|0[3|5|7|8|9])+([0-9]{8})\\b";
        Pattern pattern = Pattern.compile(regexForPhone);
        return pattern.matcher(phoneNumber).matches();
    }
}
