package com.study.exercises_2_week_2;

import java.io.*;
import java.util.*;

public class Main {
    private static int numberOfThread;
    private static Scanner scanner;
    private static ArrayList<Integer> allNumber;
    private static List<Integer> listSorted;
    private static List<SortThread> sortThreadList;
    private static List<Thread> listThread;
    
    public static void main(String[] args) throws Exception {
        scanner = new Scanner(System.in);
        allNumber = new ArrayList<>();
        listSorted = new ArrayList<>();
        sortThreadList = new ArrayList<>();
        listThread = new ArrayList<>();
        System.out.println("Enter the number of thread : ");
        while (numberOfThread < 1) {
            try {
                numberOfThread = scanner.nextInt();
            } catch (InputMismatchException e) {
                System.out.println("Invalid input");
                System.out.println("Enter your number again");
                scanner.nextLine();
            }
        }
        getListNumberFromFile();
        sortWithThread();
        for (Thread thread : listThread){
            thread.join();
        }
        new Thread(() -> {
            for (SortThread sortThread : sortThreadList){
                listSorted.addAll(sortThread.getListNumber());
            }
            Collections.sort(listSorted);
            writeToFile(listSorted);
        }).start();
    }
    
    /*
        create multi-thread to sort sublist
     */
    public static void sortWithThread(){
        int sizeOfSubList = allNumber.size() / numberOfThread;
        for (int i = 1; i <= numberOfThread; i++) {
            List<Integer> subList;
            subList = allNumber.subList((i - 1)*sizeOfSubList, i * sizeOfSubList);
            SortThread sortThread = new SortThread(subList);
            Thread thread = new Thread(sortThread);
            thread.start();
            listThread.add(thread);
            sortThreadList.add(sortThread);
        }
    }
    /*
        Function to read all number from file
     */
    public static void getListNumberFromFile() throws IOException {
        FileReader fileToRead;
        BufferedReader buffer = null;
        try {
            fileToRead = new FileReader("input.txt");
            buffer = new BufferedReader(fileToRead);
            allNumber = new ArrayList<>();
            String numberInLine;
            while ((numberInLine = buffer.readLine()) != null) {
                String[] temp = numberInLine.split(" ");
                for (String ele : temp){
                    try {
                        int num = Integer.parseInt(ele);
                        allNumber.add(num);
                    } catch (Exception e) {
                        System.out.println("File chứa ký tự");
                        System.exit(0);
                    }
                }
            }
        } catch (FileNotFoundException e) {
            System.out.println("File isn't exists");
        }
        finally {
            buffer.close();
        }
        
    }
    
    /*
        Function to write list that sorted to file
     */
    public static void writeToFile(List<Integer> listSorted) {
        try {
            FileWriter fileWriter = new FileWriter("output.txt");
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            for (Integer e : listSorted) {
                bufferedWriter.write(e.toString());
                bufferedWriter.newLine();
            }
            bufferedWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
