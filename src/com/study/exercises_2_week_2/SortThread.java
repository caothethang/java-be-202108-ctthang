package com.study.exercises_2_week_2;

import java.util.Collections;
import java.util.List;

public class SortThread implements Runnable {
    private final List<Integer> listNumber;
    
    public SortThread(List<Integer> listNumber) {
        this.listNumber = listNumber;
    }
    
    @Override
    public void run() {
        System.out.println("Thread" + Thread.currentThread().getName());
        System.out.println("Đây là của Thread: " + Thread.currentThread().getName());
        Collections.sort(listNumber);
    }
    
    public List<Integer> getListNumber() {
        return listNumber;
    }
}
