package com.study.exercises47;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    
    public static void main(String[] args) {
        Player tim = new Player("Tim", 10, 15);
        System.out.println(tim);
        saveObject(tim);
        tim.setHitPoints(8);
        System.out.println(tim);
        tim.setWeapon("Stormbringer");
        saveObject(tim);
        System.out.println(tim);
        ISaveable werewolf = new Monster("Werewolf", 20, 40);
        saveObject(werewolf);
        System.out.println(werewolf);
    }
    /*
        function to save object
     */
    public static void saveObject(ISaveable objectToSave) {
        int sizeOfObjecToSave = objectToSave.write().size();
        List<String> objectToSaveList = objectToSave.write();
        for (int i = 0; i < sizeOfObjecToSave; i++) {
            System.out.println("Saving " + objectToSaveList.get(i) + " to storage device");
        }
    }
}

